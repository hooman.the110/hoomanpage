<?php 

include $_SERVER['DOCUMENT_ROOT'].'/hoomanpage/admin/dashboard-header.php'; 
include $_SERVER['DOCUMENT_ROOT'].'/hoomanpage/functions/functions.php';

$page='index.php';

if(isset($_POST['submit'])){
        
    File::putImages("index-images",'indexpage',2500000,'index',$_POST['imagealt'],false);
    database::putTextContent('index',$_POST['text'],'',true);
    
};
if(isset($_POST['title'])){database::putPageTitle($page,$_POST['title']); } ;  /* putting page title in db */
if(isset($_POST['keywords'])){database::putKeyword($page,$_POST['keywords']); } ; /* putting meta keywords content in db*/
if(isset($_POST['description'])){database::putDescription($page,$_POST['description']); } ; /* putting meta description content in db*/
$titleValue=database::getPageTitle($page); /* getting page title from db & put it as input value */
$keywordValue=database::getKeyword($page); /* getting keyword meta content from db & put it as input value */
$descriptionValue=database::getDescription($page); /* getting description meta content from db & put it as input value */
$images=File::getImage('indexpage','index'); // getting index.php images from database 
$textResult=database::getTextContent('index')['Description'];  /*getting text from database and put it in textarea */
foreach($images as $key=>$value){
    if(isset($_POST[$value['ID']])){FILE::deleteImage($value['ID']);};
};            
?>







<h2 class="page-name">صفحه ایندکس</h2>
<form id="index-admin" action="" class="admin-index" method="post" enctype="multipart/form-data">
    <label for="title">عنوان صفحه: </label><br/>
    <input type="text" name="title" id="title" value="<?php echo "$titleValue"; ?>" class="textbox"><br/>
    <label for="keywords">کلمات کلیدی صفحه: </label><br/>
    <input type="text" name="keywords" id="keywords" value="<?php echo "$keywordValue"; ?>" class="textbox"><br/>
    <label for="description">توضیحات در مورد صفحه (description) : </label><br/>
    <input type="text" name="description" id="description" value="<?php echo "$descriptionValue"; ?>" class="textbox"><br/>
    
    
    <div class="indeximages-controller">
        <lable class="indeximage-lable">عکس های صفحه ایندکس</lable>
        <label for="image">اضافه کردن عکس به صفحه ایندکس :</label><br/>
        <input type="file" name="index-images" id="image">
        <label for="imagealt">توضیح عکس (alt) :</label>
        <input type="text" name="imagealt" value=""><br/>
        <div class="indeximage-frame">
            <p>عکس های فعلی:</p>
            <?php 
            
            foreach($images as $key=>$value){
                $optimage="http://".$_SERVER['SERVER_NAME']."/hoomanpage/upload/index/".basename($value['File']);
                echo '<div class="indeximages-box '.$value['ID'].'">';
                echo "<img src='".$optimage."'>";
                echo '<label class="remover" for="index-images">تصویر حذف شود؟</label>';
                echo '<input class="remover" type="checkbox" name="'.$value['ID'].'" value="delete-image">';
                echo '</div>';
             
            }
            ?>
        </div>
    </div>
    <label for="text">متن اصلی صفحه ایندکس :</label></br>
    <div class="index-text-content">
        <textarea id="text" rows="50" cols="100" form="index-admin" name="text"><?php echo "$textResult"; ?></textarea>
    </div>
    
<input type="submit" value="submit" class="submit" name="submit">
</form>










<?php include'dashboard-footer.php' ?>