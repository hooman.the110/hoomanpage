<?php 
include '../config.php'; 
include $root.'/admin/dashboard-header.php'; 
include $root.'/functions/functions.php'; 
$page='aboutus.php';

if(isset($_POST['submit'])){
    File::putImages('image1','about1image',1000000,'aboutus',$_POST['imagealt1'],true);
    File::putImages('image2','about2image',1000000,'aboutus',$_POST['imagealt2'],true); 
    File::putImages('imagemid','aboutmidimage',1000000,'aboutus',$_POST['imagealtmid'],true); 
    File::putImages('image-box-1','imagebox1',1000000,'aboutus',$_POST['imagebox1alt'],true); 
    File::putImages('image-box-2','imagebox2',1000000,'aboutus',$_POST['imagebox2alt'],true); 
    
    
};


if(isset($_POST['title'])){database::putPageTitle($page,$_POST['title']); } ;  /* putting page title in db */
if(isset($_POST['keywords'])){database::putKeyword($page,$_POST['keywords']); } ; /* putting meta keywords content in db*/
if(isset($_POST['description'])){database::putDescription($page,$_POST['description']); } ; /* putting meta description content in db*/
if(isset($_POST['box1text']) or isset($_POST['box2heading'])){database::putTextContent('aboutusBox1',$_POST['box1text'],$_POST['box1heading'],true);};
if(isset($_POST['box1link'])){database::putLink('aboutusBox1',$_POST['box1link'],true);}
if(isset($_POST['box2text']) or isset($_POST['box2heading'])){database::putTextContent('aboutusBox2',$_POST['box2text'],$_POST['box2heading'],true);};
if(isset($_POST['box2link'])){database::putLink('aboutusBox2',$_POST['box2link'],true);}
if(isset($_POST['customer-name']) and !empty($_POST['customer-name'])){database::putCustomers($_POST['customer-name'],$_POST['customer-link']);}


$customers=database::getCustomers();



$titleValue=database::getPageTitle($page); /* getting page title from db & put it as input value */
$keywordValue=database::getKeyword($page); /* getting keyword meta content from db & put it as input value */
$descriptionValue=database::getDescription($page); /* getting description meta content from db & put it as input value */
$about1image=File::getImage('about1image','aboutus')[0]['File']; //get current image and show in admin
$about2image=File::getImage('about2image','aboutus')[0]['File'];  //get current image and show in admin
$imagebox1=File::getImage('imagebox1','aboutus')[0]['File'];  //get current image and show in admin
$imagebox2=File::getImage('imagebox2','aboutus')[0]['File'];   //get current image and show in admin
$aboutmidimage=File::getImage('aboutmidimage','aboutus')[0]['File'];   //get current image and show in admin
$about1image=maintain::local_resorces_regulation_address($about1image,'aboutus'); //change url from localresource to prevent blocking by browser
$about2image=maintain::local_resorces_regulation_address($about2image,'aboutus'); //change url from localresource to prevent blocking by browser
$imagebox1=maintain::local_resorces_regulation_address($imagebox1,'aboutus');  //change url from localresource to prevent blocking by browser
$imagebox2=maintain::local_resorces_regulation_address($imagebox2,'aboutus');  //change url from localresource to prevent blocking by browser
$altimage1=File::getImage('about1image','aboutus')[0]['Alt']; //get alt of image from db an put in admin page
$altimage2=File::getImage('about2image','aboutus')[0]['Alt'];  //get alt of image from db an put in admin page
$altimagebox1=File::getImage('imagebox1','aboutus')[0]['Alt']; //get alt of image from db an put in admin page
$altimagebox2=File::getImage('imagebox2','aboutus')[0]['Alt'];  //get alt of image from db an put in admin page
$altimagemid=File::getImage('aboutmidimage','aboutus')[0]['Alt'];  //get alt of image from db an put in admin page
$box1text=database::getTextContent('aboutusBox1')['Description'];
$box1heading=database::getTextContent('aboutusBox1')['heading'];
$box1link=database::getLink('aboutusBox1');
$box2text=database::getTextContent('aboutusBox2')['Description'];
$box2heading=database::getTextContent('aboutusBox2')['heading'];
$box2link=database::getLink('aboutusBox2');
$aboutmidimage=maintain::local_resorces_regulation_address($aboutmidimage,'aboutus');  //change url from localresource to prevent blocking by browser


foreach($customers as $customerSerial=>$customer){
    
    if(isset($_POST[$customer['ID']])){database::deleteCustomer($customer['ID']);}
}


?>


<!--------------------------------------------Front section -------------------------------------------- -->

<form action="" class="admin-aboutus" method="post" enctype="multipart/form-data">
    <label for="title">عنوان صفحه: </label><br/>
    <input type="text" name="title" id="title" value="<?php echo "$titleValue"; ?>" class="textbox"><br/>
    <label for="keywords">کلمات کلیدی صفحه: </label><br/>
    <input type="text" name="keywords" id="keywords" value="<?php echo "$keywordValue"; ?>" class="textbox"><br/>
    <label for="description">توضیحات در مورد صفحه (description) : </label><br/>
    <input type="text" name="description" id="description" value="<?php echo "$descriptionValue"; ?>" class="textbox"><br/>

    <label for="image">عکس اول :</label><br/>
    <input type="file" name="image1" id="image">
    <img src="<?php echo "$about1image"; ?>">
    <label for="imagealt1">توضیح عکس (alt) :</label>
    <input type="text" name="imagealt1" value="<?php echo $altimage1; ?>"><br/>
    <label for="image2">عکس دوم :</label><br/>
    <input type="file" name="image2" id="image">
    <img src="<?php echo "$about2image"; ?>">
    <label for="imagealt2">توضیح عکس (alt) :</label>
    <input type="text" name="imagealt2" value="<?php echo $altimage2; ?>"><br/>

    <div class="aa-top-box">
        <label for="image-box">عکس باکس اول :</label><br/>
        <input type="file" name="image-box-1" id="image">
        <img src="<?php echo "$imagebox1"; ?>">
        <label for="imagebox1alt">توضیح عکس اول (alt) :</label>
        <input type="text" name="imagebox1alt" value="<?php echo $altimagebox1; ?>"><br/><br/>
        <label for="box1heading">تیتر باکس اول: </label>
        <input type="text" name="box1heading" value="<?php echo "$box1heading"; ?>"><br/><br/>
        <label for="box1text">متن داخل باکس اول: </label>
        <textarea class="aboutus-text-admin" name="box1text"><?php echo "$box1text";  ?></textarea>
        <label for="box1link">لینک دکمه باکس اول:</label>
        <input type="text" name="box1link" value="<?php echo "$box1link"; ?>">
    </div>

    <label for="imagemid">عکس میانی :</label><br/>
    <input type="file" name="imagemid" id="image">
    <img src=" <?php echo "$aboutmidimage" ; ?>">
    <label for="imagealtmid">توضیح عکس (alt) :</label>
    <input type="text" name="imagealtmid" value="<?php echo $altimagemid; ?>"><br/>


    <div class="aa-top-box">    
        <label for="image-box">عکس باکس دوم :</label><br/>
        <input type="file" name="image-box-2" id="image">
        <img src="<?php echo "$imagebox2"; ?>">
        <label for="imagealt1">توضیح عکس (alt) :</label>
        <input type="text" name="imagebox2alt" value="<?php echo $altimagebox2; ?>"><br/><br/>
        <label for="box2heading">تیتر باکس دوم: </label>
        <input type="text" name="box2heading" value="<?php echo "$box2heading"; ?>"><br/><br/>
        <label for="box2text">متن داخل باکس دوم: </label>
        <textarea class="aboutus-text-admin" name="box2text"><?php echo "$box2text";  ?></textarea>
        <label for="box2link">لینک دکمه باکس اول:</label>
        <input type="text" name="box2link" value="<?php echo "$box2link"; ?>">
    </div>
    


    <div class="about-customers-admin">
        <table>
            <tr>
                <th>اسم مشتری</th>
                <th>لینک</th>
            </tr>
        <?php
           
                foreach($customers as $customerSerial=>$customer){
                    echo "<tr>";
                    echo "<td>"."$customer[Customer]"."</td>";
                    echo "<td>".$customer['Link']."</td>";
                    echo "<td>";
                    echo '<label class="remover" for="index-images">مشتری حذف شود؟</label>';
                    echo '<input class="remover" type="checkbox" name="'.$customer['ID'].'" value="delete-customer">';
                    echo "</td>";
                    echo "</tr>";
                }
            
        ?>
        </table>
        <label for="customer-name" class="customer-spec">نام مشتری :</lable>
        <input type="text" name="customer-name" >
        <label for="customer-link" class="customer-spec">لینک مشتری :</lable>
        <input type="text" name="customer-link"><br/><br/>
    </div>
    <input type="submit" value="submit" class="submit" name="submit">
</form>










<?php include'dashboard-footer.php' ?>