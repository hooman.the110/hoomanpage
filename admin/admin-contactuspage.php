<?php
include $_SERVER['DOCUMENT_ROOT'].'/hoomanpage/admin/dashboard-header.php'; 
include $_SERVER['DOCUMENT_ROOT'].'/hoomanpage/functions/functions.php';

$page='contactus.php';
if(isset($_POST['submit'])){
    database::putTextContent('contactus',$_POST['text'],$_POST['text-heading'],true);
};
if(isset($_POST['title'])){database::putPageTitle($page,$_POST['title']); } ;  /* putting page title in db */
if(isset($_POST['keywords'])){database::putKeyword($page,$_POST['keywords']); } ; /* putting meta keywords content in db*/
if(isset($_POST['description'])){database::putDescription($page,$_POST['description']); } ; /* putting meta description content in db*/
$titleValue=database::getPageTitle($page); /* getting page title from db & put it as input value */
$keywordValue=database::getKeyword($page); /* getting keyword meta content from db & put it as input value */
$descriptionValue=database::getDescription($page); /* getting description meta content from db & put it as input value */
$textResult=database::getTextContent('contactus'); /*getting text from database and put it in textarea */
?>
<h2 class="page-name">صفحه تماس با من</h2>
<form id="contact-admin" action="" class="admin-contact" method="post" enctype="multipart/form-data">
    <label for="title">عنوان صفحه: </label><br/>
    <input type="text" name="title" id="title" value="<?php echo "$titleValue"; ?>" class="textbox"><br/>
    <label for="keywords">کلمات کلیدی صفحه: </label><br/>
    <input type="text" name="keywords" id="keywords" value="<?php echo "$keywordValue"; ?>" class="textbox"><br/>
    <label for="description">توضیحات در مورد صفحه (description) : </label><br/>
    <input type="text" name="description" id="description" value="<?php echo "$descriptionValue"; ?>" class="textbox"></br></br>

<div class="index-text-content">
    <label for="text-heading">تیتر بالای متن :</label><br/>
    <input type="text" name="text-heading" id="text-heading" value="<?php echo $textResult['heading']; ?>" class="textbox"></br></br>
    <label for="text">متن بالای صفحه تماس با من :</label></br>
    <textarea id="text" rows="50" cols="100" form="contact-admin" name="text"><?php echo $textResult['Description']; ?></textarea>
    </div>
    
<input type="submit" value="submit" class="submit" name="submit">





<?php include'dashboard-footer.php' ?>