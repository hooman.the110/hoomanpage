<?php
include '../config.php'; 
include $root.'/admin/dashboard-header.php'; 
include $root.'/functions/functions.php'; 
$page='multimedia.php';

if(isset($_POST['title'])){database::putPageTitle($page,$_POST['title']); } ;  /* putting page title in db */
if(isset($_POST['keywords'])){database::putKeyword($page,$_POST['keywords']); } ; /* putting meta keywords content in db*/
if(isset($_POST['description'])){database::putDescription($page,$_POST['description']); } ; /* putting meta description content in db*/

$titleValue=database::getPageTitle($page); /* getting page title from db & put it as input value */
$keywordValue=database::getKeyword($page); /* getting keyword meta content from db & put it as input value */
$descriptionValue=database::getDescription($page); /* getting description meta content from db & put it as input value */
if(isset($_POST['submit']) and !empty($_POST['video-link'])){ // put video to database
    if(isset($_POST['video-active'])){$videoActive=true;}else{$videoActive=false;}
    database::putVideo($link=$_POST['video-link'],$heading=$_POST['video-heading'],$duration=$_POST['video-duration'],$comment=$_POST['video-comment'],$jy=$_POST['video-year']
        ,$jm=$_POST['video-month'],$jd=$_POST['video-day'],$videoActive);
}

//get active page number from url string
parse_str($_SERVER['QUERY_STRING'],$queries);
if(empty($queries)){$queries['pageination']=1;}
$activePage=$queries['pageination'];

//pageination initializing
$numberOfVideosInPage=10;
$justActiveVideos=false;
$videoList=database::getVideoList($justActiveVideos,$numberOfVideosInPage,$activePage);
$numberOfVideoPages=database::videoPagination($justActiveVideos,$numberOfVideosInPage);


?>

<!--------------------------------------------Front section -------------------------------------------- -->

<form action="" class="admin-aboutus" method="post" enctype="multipart/form-data">
    <label for="title">عنوان صفحه: </label><br/>
    <input type="text" name="title" id="title" value="<?php echo "$titleValue"; ?>" class="textbox"><br/>
    <label for="keywords">کلمات کلیدی صفحه: </label><br/>
    <input type="text" name="keywords" id="keywords" value="<?php echo "$keywordValue"; ?>" class="textbox"><br/>
    <label for="description">توضیحات در مورد صفحه (description) : </label><br/>
    <input type="text" name="description" id="description" value="<?php echo "$descriptionValue"; ?>" class="textbox"><br/>
    
    <div class="admin-input-video">
        <div class="admin-multimedia-form-input-divider">
            <label for="video-heading">عنوان ویدئو: </label><br/>
            <input type="text" name="video-heading" id="video-heading" value="" >
        </div>
        <div class="admin-multimedia-form-input-divider">
            <label for="video-link">لینک ویدئو: </label><br/>
            <input type="text" name="video-link" id="video-link" value="" >
        </div>    
        <div class="admin-multimedia-form-input-divider">
            <label for="video-duration">مدت ویدئو: </label><br/>
            <input type="text" name="video-duration" id="video-duration" value="" >
        </div>
        <div class="admin-multimedia-form-input-divider">    
            <label for="video-comment">درمورد ویدئو: </label><br/>
            <input type="text" name="video-comment" id="video-comment" value="" >
        </div>    
        <div class="admin-multimedia-form-input-divider">    
            <label for="video-date">تاریخ ویدئو: </label><br/>
            <select name="video-year">
                <option value="1396">1396</option>
                <option value="1397">1397</option>
                <option value="1398">1398</option>
                <option value="1399">1399</option>
                <option value="1400">1400</option>
            </select>
            <select name="video-month">
                <option value="1">فروردین</option>
                <option value="2">اردیبهشت</option>
                <option value="3">خرداد</option>
                <option value="4">تیر</option>
                <option value="5">مرداد</option>
                <option value="6">شهریور</option>
                <option value="7">مهر</option>
                <option value="8">آبان</option>
                <option value="9">آذر</option>
                <option value="10">دی</option>
                <option value="11">بهمن</option>
                <option value="12">اسفند</option>
            </select>
            <select name="video-day">
            <?php
                for($i=1;$i<=31;$i++){
                    echo "<option value='".$i."'>".$i."</option>";
                }
            ?>
            </select>
        </div>  
        <div class="admin-multimedia-form-input-divider-checkbox">
            <input type="checkbox" id="video-active" name="video-active" value="video-active">
            <label for="video-active">فعال بودن ویدئو</label><br/><br/>  
        </div>    
    </div>

    <input type="submit" value="submit" class="submit" name="submit">
</form>

<div class="multimedia-list">
    <h2>لیست ویدئوها</h2> 
    <table>
        <tr>
            <th>ID</th>
            <th>عنوان</th>
            <th>فعال بودن</th>
            <th>کامنت</th>
            <th>تاریخ</th>
            <th>مدت</th>
            <th>ویرایش</th>
        </tr>
        <?php 
            foreach($videoList as $row=>$video){

                echo "<tr>";
                    echo "<td>".$video['ID']."</td>";
                    echo "<td>".$video['Heading']."</td>";
                    echo "<td>".$video['Active']."</td>";
                    echo "<td>".$video['Comment']."</td>";
                    echo "<td>".$video['Date']."</td>";
                    echo "<td>".$video['Duration']."</td>";
                    echo "<td>".'ویرایش'."</td>";

                echo "</tr>";
            }
        ?>
        
    </table>
    <div class="admin-video-pagination">
        <?php
            for($i=1;$i<=$numberOfVideoPages;$i++){
                echo "<a href='?pageination=".$i."' id='pageination".$i."'>".$i."</a>";
            }
        ?>
    </div>
</div>









<?php include'dashboard-footer.php' ?>