<?php
session_start();
if($_SESSION['eligible']!==true){header("location:../login.php");}
?>
<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="robots" content="noindex">
        <title>پنل مدیریت</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../index.css">
        <link href="../css/all.css" rel="stylesheet">
        <script src="<?php echo "http://".$_SERVER['SERVER_NAME'].'/hoomanpage/ckeditor/ckeditor.js'; ?>"></script>
        
    </head>
    <script>
        var loadFile = function(event) {
        var output = document.getElementById('blog-image-output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        }
        };
    </script>
    <body>
        <header class="adm-header">
            پنل مدیریت وب سایت هومن ممتحنی
            <a class="log-out" href="../logout.php"><i class="fas fa-sign-out-alt"></i></a>
        </header>
        <div class="adm-right-sider">
            <button class="accordion">صفحات</button>
            <div class="panel">
                <ul>
                    <li><a href="admin-indexpage.php">ایندکس</a></li>
                    <li><a href="admin-contactuspage.php">تماس با من</a></li>
                    <li><a href="admin-aboutuspage.php">درباره من</a></li>
                    <li><a href="admin-bloglistpage.php">بلاگ</a></li>
                    <li><a href="admin-multimediapage.php">مولتی مدیا</a></li>
                </ul>
            </div>
            <button class="accordion">تنظیمات</button>
            <div class="panel">
            <a href="settings.php">تنظیمات</a>
            <br/>
            <a href="newsletter.php"> خبرنامه</a>
            <br/>
            <a href="messages.php">فرم تماس</a>
            </div>
            <button class="accordion">پنل سوم</button>
            <div class="panel">
                تست تست تست
            </div>
        </div>
        <div class="adm-main">
        
        