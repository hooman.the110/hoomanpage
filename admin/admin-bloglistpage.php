<?php 
include '../config.php'; 
include $root.'/admin/dashboard-header.php'; 
include $root.'/functions/functions.php'; 
$page='blog.php';

if(isset($_POST['title'])){database::putPageTitle($page,$_POST['title']); } ;  /* putting page title in db */
if(isset($_POST['keywords'])){database::putKeyword($page,$_POST['keywords']); } ; /* putting meta keywords content in db*/
if(isset($_POST['description'])){database::putDescription($page,$_POST['description']); } ; /* putting meta description content in db*/

$default_image_add=maintain::local_resorces_regulation_address('C:/wamp64/www/hoomanpage/upload/etc/image-default.png','etc');
$titleValue=database::getPageTitle($page); /* getting page title from db & put it as input value */
$keywordValue=database::getKeyword($page); /* getting keyword meta content from db & put it as input value */
$descriptionValue=database::getDescription($page); /* getting description meta content from db & put it as input value */

if(isset($_POST['submit']) && !empty($_POST['blog-title']) && !empty($_POST['blog-category']) && !empty($_POST['blog-link'])){
    
    if(isset($_POST['blog-active'])){$blogActive=true;}else{$blogActive=false;}
    File::putBlog($_POST['blog-title'],$_POST['blog-category'],$_POST['blog-link'],'blog-image',$blogActive);
}
//get active page number from url string
parse_str($_SERVER['QUERY_STRING'],$queries);
if(empty($queries)){$queries['pageination']=1;}
$activePage=$queries['pageination'];

//pageination initializing
$numberOfBlogsInPage=10;
$category='all';
$justActiveBlogs=false;
$numberOfPages=database::blogPagination($category,$justActiveBlogs,$numberOfBlogsInPage);
$blogList=database::getBlogList($category,$justActiveBlogs,$numberOfBlogsInPage,$activePage);


?>





<h2 class="page-name">صفحه بلاگ</h2>
<form id="index-admin" action="" class="admin-index" method="post" enctype="multipart/form-data">
    <label for="title">عنوان صفحه: </label><br/>
    <input type="text" name="title" id="title" value="<?php echo "$titleValue"; ?>" class="textbox"><br/>
    <label for="keywords">کلمات کلیدی صفحه: </label><br/>
    <input type="text" name="keywords" id="keywords" value="<?php echo "$keywordValue"; ?>" class="textbox"><br/>
    <label for="description">توضیحات در مورد صفحه (description) : </label><br/>
    <input type="text" name="description" id="description" value="<?php echo "$descriptionValue"; ?>" class="textbox"><br/>
    <div class="blog-input">
        <h2>درج مقاله جدید</h2>
        <label for="blog-title">عنوان مقاله: </label><br/>
        <input type="text" name="blog-title" id="blog-title" value="" class="textbox"><br/>
        <label for="blog-category">موضوع مقاله: </label><br/>
        <input type="text" name="blog-category" id="blog-category" value="" class="textbox"><br/>
        <label for="blog-link">لینک مقاله: </label><br/>
        <input type="text" name="blog-link" id="blog-link" value="" class="textbox"><br/><br/>
        <input type="checkbox" id="blog-active" name="blog-active" value="blog-active">
        <label for="blog-active">فعال بودن مقاله</label><br/><br/>
        <label for="blog-image">تصویر مقاله</label>
        <input type="file" id="blog-image" name="blog-image" onchange="loadFile(event)">
        <img src="<?php echo $default_image_add;  ?>" id="blog-image-output" class="blog-image-output">
    </div>
    <input type="submit" value="submit" class="submit" name="submit">
</form>

<div class="blog-list">
    <h2>لیست مقالات</h2> 
    <table>
        <tr>
            <th>ID</th>
            <th>عنوان</th>
            <th>موضوع</th>
            <th>فعال بودن</th>
            <th>عکس</th>
            <th>ویرایش</th>
        </tr>
        <?php
            foreach($blogList as $blogSerial=>$blog){
                if($blog['Isactive']=='1'){$blogActivity='فعال';}else{$blogActivity='غیر فعال';}
                $blogImageAdd=maintain::local_resorces_regulation_address($blog['Image'],'blog');
                echo "<tr>";
                echo "<td>".$blog['ID']."</td>";
                echo "<td>".$blog['Heading']."</td>";
                echo "<td>".$blog['Category']."</td>";
                echo "<td>".$blogActivity."</td>";
                echo "<td><img class='blog-small' src='".$blogImageAdd."'></td>";
                echo "<td>"."<a href='admin-blogeditpage.php?id=".$blog['ID']."'>".'<i class="fas fa-edit"></i>'."</a>"."</td>";
                echo "</tr>";
            } 
        ?>
        
    </table>
    <div class="admin-blog-pagination">
        <?php
            for($i=1;$i<=$numberOfPages;$i++){
                echo "<a href='?pageination=".$i."' id='pageination".$i."'>".$i."</a>";
            }
        ?>
    </div>
</div>




<?php include'dashboard-footer.php' ?>
