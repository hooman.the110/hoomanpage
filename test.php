<?php

// date_default_timezone_set('Asia/Tehran');

$now = new DateTime();

$formatter = new IntlDateFormatter(
                "fa_IR@calendar=persian", 
                IntlDateFormatter::FULL, 
                    IntlDateFormatter::FULL, 
                'Asia/Tehran', 
                IntlDateFormatter::TRADITIONAL, 
                "yyyy-MM-dd");

// It is now: 1396-12-14
echo 'It is now: ' . convert_to_number($formatter->format($now)) ."<br />"; 

// It is now: ۱۳۹۶-۱۲-۱۴
echo 'It is now: ' . $formatter->format($now) ."<br />"; 


// convert to number
function convert_to_number($string) {
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);
    $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

    return $englishNumbersOnly;
}
?>