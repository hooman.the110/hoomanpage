<?php
include 'header.php';  
include 'config.php'; 


?>
<div class="contact-main">
    <div class="contact-top">
        <h2>
        <?php echo $contactusTextResult['heading']; ?>
        </h2>
        <p>
        <?php echo $contactusTextResult['Description']; ?>
        </p>
    </div>
    
    <div class="seperator-line"></div>
    <div class="contact-form">
    <p class="social">
            <a href="<?php echo $instagramAddress; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
            <a href="<?php echo $linkedinAddress; ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
            </p>
        <h3>از طریق فرم زیر با من تماس بگیرید</h3>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data" id="contactus">
            <div class="form-fname">
                نام : <br/>
                <input name="fname" class="fname" required>
            </div>
            <div class="form-lname">
                نام خانوادگی : <br/>
                <input name="lname" class="lname">
            </div>
            <div class="form-email">
                ایمیل : <br/>
                <input name="emailadd" class="emailadd" required>
            </div>
            <div class="form-subject">
                موضوع : <br/>
                <input name="subject" class="subject">
            </div>
            <div class="form-message">
                متن پیغام : <br/>
                <textarea form="contactus" name="message" class="message" autofocus required></textarea>
            </div>
            <input type="submit" value="ارسال" name="contact-submit">
        </form>
    </div>
</div>

<?php include 'footer.php'; ?>