
<p class="borderline"></p>
<footer>
            <div class="subscribe">
                <h3>خبرنامه</h3>
                ایمیلتون رو وارد کنید تا جدیدترین مقالات رو براتون بفرستم
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <input type="text" id="email" name="email">
                    <input type="submit" value="تایید ایمیل" name="submit">
                </form>
            </div>
            <p class="copyright">
                استفاده از مطالب با ذکر منبع مجاز است
            </p>
            <p class="social">
            <a href="<?php echo $instagramAddress; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
            <a href="<?php echo $linkedinAddress; ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
            </p>
        </footer>
</div>
    </body>
    <script src="js/index.js"></script>
</html>