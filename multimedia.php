<?php include 'header.php'?>



<div class="highlight-multimedia">
<h2><?php echo $videoList[0]['Heading']; ?></h2>
<p class="video-spec">
    <i class="fas fa-stopwatch time"></i><?php echo $videoList[0]['Duration']; ?>
    <br/>
    <i class="far fa-calendar-alt time"></i><?php echo $videoList[0]['Date']; ?>
</p>
<?php echo $videoList[0]['Link']; ?>
</div>
<div class="multimedia-posts">
    <?php 
        foreach($videoList as $row=>$video){
            if($row<1) continue;
            echo "<div class='videobox'>";
            echo "<div class='inserted-video'>";
            echo $video['Link'];
            echo "</div>";
            echo "<p class='title'>".$video['Heading']."</p>";
            echo "<p class='video-spec'>";
                echo '<i class="fas fa-stopwatch time"></i>'.$video['Duration'];
                echo "<br/>";
                echo '<i class="far fa-calendar-alt time"></i>'.$video['Date'];
            echo "</p>";
                
            echo "</div>";    
        }

    ?>
    <br/>
    <div class="admin-video-pagination">
        <?php
            
            for($i=1;$i<=$numberOfVideoPages;$i++){
                echo "<a href='?pageination=".$i."' id='pageination".$i."'>".$i."</a>";
            }
        ?>
    </div>
    
</div>



<?php include 'footer.php'; ?>