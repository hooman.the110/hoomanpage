<!DOCTYPE html>
<?php
include $_SERVER['DOCUMENT_ROOT'].'/hoomanpage/db/header-db.php';
?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo "$titleResult" ?></title>
        <meta name="keywords" content="<?php echo "$keywordResult" ?>">
        <meta name="description" content="<?php echo "$descriptionResult" ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="index.css">
        <link href="css/all.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.mosaic.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery.mosaic.min.css" />
       
    </head>
    <body>
        
        <div class="width-limiter">
        <header>
            
            <p class="text">
            <i class="fas fa-bars" id="menu-button" onclick="openmenu()"></i>
            </p>
            <p class="borderline top"></p>  
            <div class="responsive-menu" id="responsive-menu">
            <?php foreach($navigationPages as $result){
                $url=$result['Url'];
                $pagename=$result['PageName'];
                echo "<span>"."<a href='"."$url"."'>$pagename</a>"."</span>";
                } ?>
                <p class="close"><i class="fas fa-times" id="close-button" onclick="closemenu()"></i></p> 
            </div> 
            <p class="image">
                
                <img src="<?php
                $headerimage=maintain::local_resorces_regulation_address($headerimage,'');
                echo "$headerimage" ;
                ?>" alt="هومن ممتحنی">

            </p> 
           
            <div class="wide-menu">
            <?php foreach($navigationPages as $result){
                $url=$result['Url'];
                $pagename=$result['PageName'];
                echo " <span> "." <a href='"."$url"."'>$pagename</a> "." </span> ";
                } ?>
                
            </div>
            <p class="borderline"></p>
        </header>