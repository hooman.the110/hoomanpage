<?php

include $_SERVER['DOCUMENT_ROOT'].'/hoomanpage/variabales.php';

/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------database  class --------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
class database{
    public static function safeInput($input){
        $input=str_replace("'","&#39",$input);
        $input=str_replace('"',"&#34",$input);
        return $input;
    }

    public static function safeOutput(){

    }

    /* ------------ get title from admin form & put to database function ------------------- */

    public static function putPageTitle($page,$title){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        if(isset($title)){
            $title=self::safeInput($title);
            try{
                $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $stmt=$dbconn->prepare("DELETE FROM titletags WHERE Page=:page");
                $stmt->bindParam(':page',$pagedb);
                $pagedb=$page;
                $stmt->execute();
                $stmt=$dbconn->prepare("INSERT INTO titletags(Page,TitleValue) VALUES(:page, :title)");
                $stmt->bindParam(':page',$pagedb);
                $stmt->bindParam(':title',$titledb);
                $pagedb=$page;
                $titledb=$title;
                $stmt->execute();
                Notifications::Modal("تغییرات با موفقیت انجام شد!");
            }
            catch(PDOException $e){
                Notifications::Modal($e->getMessage());

            }
            $dbconn=null;
            
        }
    
    }
    /* ------------ get titlepage from database function ------------------- */

    public static function getPageTitle($currentPage){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        try {
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("SELECT TitleValue FROM titletags WHERE page=:currentPagedb");
            $stmt->bindParam(':currentPagedb',$currentPagedb);
            $currentPagedb=$currentPage;
            $stmt->execute();
            $result=$stmt->fetch($dbconn::FETCH_ASSOC);
            $titleResult=$result['TitleValue'];
            $titleResult=str_replace('"',"&#34",$titleResult);
            $titleResult=str_replace("'","&#39",$titleResult);
            return $titleResult;
            
        }
        catch(PDOException $e) {
            Notifications::Modal($e->getMessage());

        }
        $dbconn = null;
        
        }

        
   

    /* ------------ get keyword for meta tag from admin form & put to database function ------------------- */

    public static function putKeyword($page,$keyword){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        if(isset($keyword)){
            $keyword=self::safeInput($keyword);
            try{
                $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $stmt=$dbconn->prepare("DELETE FROM keywordtags WHERE Page=:page");
                $stmt->bindParam(':page',$pagedb);
                $pagedb=$page;
                $stmt->execute();
                $stmt=$dbconn->prepare("INSERT INTO keywordtags(Page,KeywordValue) VALUES(:page, :keyword)");
                $stmt->bindParam(':page',$pagedb);
                $stmt->bindParam(':keyword',$keyworddb);
                $pagedb=$page;
                $keyworddb=$keyword;
                $stmt->execute();
                Notifications::Modal("تغییرات با موفقیت انجام شد!");
            }
            catch(PDOException $e){
                Notifications::Modal($e->getMessage());
            }
            $dbconn=null;
            
    };
    }


    /* ------------ get meta keywords from database function ------------------- */
    public static function getKeyword($currentPage){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        try {
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("SELECT KeywordValue FROM keywordtags WHERE page=:page");
            $stmt->bindParam(':page',$pagedb);
            $pagedb=$currentPage;
            $stmt->execute();
            $result=$stmt->fetch($dbconn::FETCH_ASSOC);
            $keywordResult=$result['KeywordValue'];
            $keywordResult=str_replace('"',"&#34",$keywordResult);
            $keywordResult=str_replace("'","&#39",$keywordResult);
            return $keywordResult;
            
        }
        catch(PDOException $e) {
            Notifications::Modal($e->getMessage());

        }
        $dbconn = null;
    
    }
    /* ------------ get description for meta tag from admin form & put to database function ------------------- */

    public static function putDescription($page,$description){
            global $dbservername,$dbusername,$dbpassword,$dbname;
            if(isset($description)){
                $description=self::safeInput($description);
                try{
                    $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                    $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                    $stmt=$dbconn->prepare("DELETE FROM descriptiontags WHERE Page=:page");
                    $stmt->bindParam(':page',$pagedb);
                    $pagedb=$page;
                    $stmt->execute();
                    $stmt=$dbconn->prepare("INSERT INTO descriptiontags(Page,DescriptionValue) VALUES(:page,:description)");
                    $stmt->bindParam(':page',$pagedb);
                    $stmt->bindParam(':description',$descriptiondb);
                    $pagedb=$page;
                    $descriptiondb=$description;
                    $stmt->execute();
                    Notifications::Modal("تغییرات با موفقیت انجام شد!");
                }
                catch(PDOException $e){
                    Notifications::Modal($e->getMessage());

                }
                $dbconn=null;
                
            };
    }

    /* ------------ get meta keywords from database function ------------------- */
    public static function getdescription($currentPage){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        try {
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("SELECT DescriptionValue FROM descriptiontags WHERE page=:currentPage");
            $stmt->bindParam(':currentPage',$currentPage);
            $currentPagedb=$currentPage;
            $stmt->execute();
            $result=$stmt->fetch($dbconn::FETCH_ASSOC);
            $descriptionResult=$result['DescriptionValue'];
            $descriptionResult=str_replace('"',"&#34",$descriptionResult);
            $descriptionResult=str_replace("'","&#39",$descriptionResult);
            return $descriptionResult;
            
        }
        catch(PDOException $e) {
            Notifications::Modal($e->getMessage());
        }
        $dbconn = null;
    
    }

/* ---------------------------------------- activate page from admin form and put "checked"  on database --------------------------- */
    public static function ActivePage($page){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        if(isset($page)){ 
            try{
                    $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                    $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                    $sql="UPDATE topnavigation SET active='checked' WHERE URL='$page'";
                    $dbconn->exec($sql);
                    Notifications::Modal("تغییرات با موفقیت انجام شد!");
                }
            catch(PDOException $e){
                Notifications::Modal("$sql.<br/>.$e->getMessage()");
            }
            $dbconn=null;
                
            };        



    }


/* ---------------------------------------- deactivate page from admin form and put ' '  on database --------------------------- */
public static function DeactivePage($page){
    global $dbservername,$dbusername,$dbpassword,$dbname;
        
        try{
                $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $sql="UPDATE topnavigation SET active=' ' WHERE URL='$page'";
                $dbconn->exec($sql);
                Notifications::Modal("تغییرات با موفقیت انجام شد!");
            }
        catch(PDOException $e){
            Notifications::Modal("$sql.<br/>.$e->getMessage()");
        }
        $dbconn=null;
            
        }       

/* ---------------------------------------- get array of active pages in navigation menu --------------------------- */

public static function getNavigationPages(){
    global $dbservername,$dbusername,$dbpassword,$dbname;
        try {
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("SELECT Url,PageName From topnavigation WHERE active='checked'");
            $stmt->execute();
            $resultArray=$stmt->setFetchMode(PDO::FETCH_ASSOC);
            $resultArray=$stmt->fetchAll();
            return $resultArray;
            
        }
        catch(PDOException $e) {
            Notifications::Modal($e->getMessage());
        }
        $dbconn = null;
}

/*------------------------------------ getting email from footer form for newsletter and put it on database ----------------*/
public static function getEmail($email){
    global $dbservername,$dbusername,$dbpassword,$dbname;

       date_default_timezone_get("Asia/Tehran");
       $date=date("Y/m/d");
        if($email!=null && filter_var($email, FILTER_VALIDATE_EMAIL)){
            //check if email exists in databse
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("SELECT Email FROM newsletter WHERE Email='$email'");
            $stmt->execute();
            $result=$stmt->fetchAll();
            $checkIndex=$result;
            
                if($checkIndex==null){
                    try{                             //put email in database
                        $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $stmt=$dbconn->prepare("INSERT INTO newsletter(Email,Date) VALUES(:emaildb, :datedb)");
                        $stmt->bindParam(':emaildb',$emaildb);
                        $stmt->bindParam(':datedb',$datedb);
                        $emaildb=$email;
                        $datedb=$date;
                        $stmt->execute();
                        Notifications::Modal("!ایمیل شما ثبت شد");
                    }
                    catch(PDOException $e) {
                    
                        Notifications::Modal($e->getMessage());
                    }
                    $dbconn=null;
                }else{
                    Notifications::Modal("ایمیل وارد شده تکراری است");
                }
            }else{
            Notifications::Modal("خطا : لطفا یک ایمیل صحیح وارد کنید");
            };
     
}

/* --------------------------------- showing newsletter members in admin newsletter.php -----------------------*/
public static function newsletterMembers(){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt=$dbconn->prepare("SELECT Email,Date FROM newsletter");
        $stmt->execute();
        $result=$stmt->fetchAll();
        return $result;
        
    }
    catch(PDOException $e) {
        Notifications::Modal($e->getMessage());

    }
    $dbconn = null;

}

/*---------------------------------getting social network addresses from admin page and put it on database ----------------*/

public static function putSocialAddress($social,$Address){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    if(isset($Address)){
        $Address=self::safeInput($Address);
        try{
            $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("DELETE FROM socialaddress WHERE Social=:social");
            $stmt->bindParam(':social',$socialdb);
            $socialdb=$social;
            $stmt->execute();
            $stmt=$dbconn->prepare("INSERT INTO socialaddress(Social,Address) VALUES(:social,:address)");
            $stmt->bindParam(':social',$socialdb);
            $stmt->bindParam(':address',$addressdb);
            $socialdb=$social;
            $addressdb=$Address;
            $stmt->execute();
            Notifications::Modal("تغییرات با موفقیت انجام شد!");
        }
        catch(PDOException $e){
            Notifications::Modal($e->getMessage());

        }
        $dbconn=null;
        
    };
}

/*---------------------------------getting social network addresses from database ----------------*/

public static function getSocialAddress($social){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt=$dbconn->prepare("SELECT Address FROM socialaddress WHERE Social=:social");
        $stmt->bindParam(':social',$socialdb);
        $socialdb=$social;
        $stmt->execute();
        $result=$stmt->fetch($dbconn::FETCH_ASSOC);
        $titleResult=$result['Address'];
        $titleResult=str_replace('"',"&#34",$titleResult);
        $titleResult=str_replace("'","&#39",$titleResult);
        return $titleResult;
        
    }
    catch(PDOException $e) {
        Notifications::Modal($e->getMessage());

    }
    $dbconn = null;
    
    }
   
/*---------------------------------getting pages text contents from admin pages and put it on database ----------------*/    
public static function putTextContent($whereToUse,$description,$heading,$shouldReplace){
    global $dbservername,$dbusername,$dbpassword,$dbname;
        $description=htmlspecialchars($description);
        $heading=htmlspecialchars($heading);
        try{
            
            $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            if($shouldReplace){
                $stmt=$dbconn->prepare("DELETE FROM pagetextcontent WHERE Wheretouse=:wheretouse");
                $stmt->bindParam(':wheretouse',$wheretousedb);
                $wheretousedb=$whereToUse;
                $stmt->execute();    
            }
            
            $stmt=$dbconn->prepare("INSERT INTO pagetextcontent(Wheretouse,Description,heading) VALUES(:wheretouse,:description,:heading)");
            $stmt->bindParam(':wheretouse',$wheretousedb);
            $stmt->bindParam(':description',$descriptiondb);
            $stmt->bindParam(':heading',$headingdb);
            $wheretousedb=$whereToUse;
            $descriptiondb=$description;
            $headingdb=$heading;
            $stmt->execute();

            Notifications::Modal("تغییرات با موفقیت انجام شد!");

            
        }
        catch(PDOException $e){
            echo ($e);
             

        }
        $dbconn=null;
        
    
}
/*---------------------------------getting pages text contents from database ----------------*/

public static function getTextContent($whereToUse){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt=$dbconn->prepare("SELECT Description,heading FROM pagetextcontent WHERE Wheretouse=:wheretouse");
        $stmt->bindParam(':wheretouse',$wheretousedb);
        $wheretousedb=$whereToUse;
        $stmt->execute();
        $result=$stmt->fetch($dbconn::FETCH_ASSOC);
        $textResult=$result;

        return $textResult;
        
    }
    catch(PDOException $e) {
        Notifications::Modal($e->getMessage());

    }
    $dbconn = null;
    
    }

    
/*---------------------------------getting contact form from contactus page and put it on database ----------------*/    
public static function putContactForm($name,$family,$email,$subject,$message){
    global $dbservername,$dbusername,$dbpassword,$dbname;
        $name=htmlspecialchars($name);
        $family=htmlspecialchars($family);
        $email=htmlspecialchars($email);
        $subject=htmlspecialchars($subject);
        $message=htmlspecialchars($message);
        try{
            
            
            $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("INSERT INTO contactform(Name,Family,Email,Subject,Message) VALUES(:name,:family,:email,:subject,:message)");
            $stmt->bindParam(':name',$namedb);
            $stmt->bindParam(':family',$familydb);
            $stmt->bindParam(':email',$emaildb);
            $stmt->bindParam(':subject',$subjectdb);
            $stmt->bindParam(':message',$messagedb);
            $namedb=$name;
            $familydb=$family;
            $emaildb=$email;
            $subjectdb=$subject;
            $messagedb=$message;
            $stmt->execute();
            Notifications::Modal(":) پیغام شما با موفقیت ارسال شد");
    

            
        }
        catch(PDOException $e){
            echo ($e);
             

        }
        $dbconn=null;
        
    
} 

/* --------------------------------- showing contactus messages in admin messages.php -----------------------*/
public static function showingMessages(){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt=$dbconn->prepare("SELECT ID,Name,Family,Email,Subject,Message FROM contactform");
        $stmt->execute();
        $result=$stmt->fetchAll();
        return $result;
        
    }
    catch(PDOException $e) {
        Notifications::Modal($e->getMessage());

    }
    $dbconn = null;

}



/*---------------------------------getting links from admin pages and put it on database ----------------*/    
public static function putLink($whereToUse,$link,$shouldReplace){
    global $dbservername,$dbusername,$dbpassword,$dbname;
        $link=maintain::modification_input_url($link);
        $whereToUse=htmlspecialchars($whereToUse);
       
        try{
            
            $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            if($shouldReplace){
                $stmt=$dbconn->prepare("DELETE FROM links WHERE Wheretouse=:wheretouse");
                $stmt->bindParam(':wheretouse',$wheretousedb);
                $wheretousedb=$whereToUse;
                $stmt->execute();    
            }
            
            $stmt=$dbconn->prepare("INSERT INTO links(Wheretouse,Link) VALUES(:wheretouse,:link)");
            $stmt->bindParam(':wheretouse',$wheretousedb);
            $stmt->bindParam(':link',$linkdb);
            $wheretousedb=$whereToUse;
            $linkdb=$link;
            $stmt->execute();

            Notifications::Modal("تغییرات با موفقیت انجام شد!");

            
        }
        catch(PDOException $e){
            echo ($e);
             

        }
        $dbconn=null;
        
    
}

/*---------------------------------getting links from database ----------------*/

public static function getLink($whereToUse){

    global $dbservername,$dbusername,$dbpassword,$dbname;
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt=$dbconn->prepare("SELECT Link FROM links WHERE Wheretouse=:wheretouse");
        $stmt->bindParam(':wheretouse',$wheretousedb);
        $wheretousedb=$whereToUse;
        $stmt->execute();
        $result=$stmt->fetch($dbconn::FETCH_ASSOC);
        $textResult=$result;
        return $textResult['Link'];
        
    }
    catch(PDOException $e) {
        Notifications::Modal($e->getMessage());

    }
    $dbconn = null;
    
}  


/*---------------------------------getting customer name & links from admin pages and put it on database ----------------*/    
public static function putCustomers($customer,$link){
    global $dbservername,$dbusername,$dbpassword,$dbname;
        $link=maintain::modification_input_url($link);
        $whereToUse=htmlspecialchars($customer);
       
        try{
            
            $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            
            $stmt=$dbconn->prepare("INSERT INTO customers(Customer,Link) VALUES(:customer,:link)");
            $stmt->bindParam(':customer',$customerdb);
            $stmt->bindParam(':link',$linkdb);
            $customerdb=$customer;
            $linkdb=$link;
            $stmt->execute();

            Notifications::Modal("تغییرات با موفقیت انجام شد!");

            
        }
        catch(PDOException $e){
            echo ($e);
             

        }
        $dbconn=null;
        
    
}

/*---------------------------------getting customers name and link from database ----------------*/

public static function getCustomers(){

    global $dbservername,$dbusername,$dbpassword,$dbname;
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt=$dbconn->prepare("SELECT * FROM customers");
        $stmt->execute();
        $result=$stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result=$stmt->fetchAll();
        return $result;
        
    }
    catch(PDOException $e) {
        Notifications::Modal($e->getMessage());

    }
    $dbconn = null;
    
}  

/*------------------------------------------delete customer from database function -------------------------------------*/
public static function deleteCustomer($id){
    
    global $dbservername,$dbusername,$dbpassword,$dbname;
        try {
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("DELETE FROM customers WHERE ID=:id");
            $stmt->bindParam(':id',$iddb);
            $iddb=$id;
            $stmt->execute();
            
        }
        catch(PDOException $e) {
            $msg="Error: " . $e->getMessage();
            Notifications::Modal("$msg");
        }
        $dbconn = null;
        
        echo "<meta http-equiv='refresh' content='0'>";
}

/* ------------ get blog specifications from database function ------------------- */
public static function getBlogList($category,$justActives,$quantityOfResults,$rowOfResult){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    $offset=($rowOfResult-1)*$quantityOfResults;
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if($category!=='all'){
            $stmt=$dbconn->prepare("SELECT ID,Heading,Category,Link,Image,Isactive FROM blogs ORDER BY ID WHERE Category=:category ");
            $stmt->bindParam(':category',$categorydb);
            $categorydb=$category;
        }else{
            switch($justActives){
                case false:
                    $stmt=$dbconn->prepare("SELECT ID,Heading,Category,Link,Image,Isactive FROM blogs ORDER BY ID DESC LIMIT :offset,:quantityOfResults");
                    $stmt->bindParam(':quantityOfResults',$quantityOfResultsdb);
                    $quantityOfResultsdb=$quantityOfResults;
                    $stmt->bindParam(':offset',$offsetdb);
                    $offsetdb=$offset;
                    
                    break;
                default:
                $stmt=$dbconn->prepare("SELECT ID,Heading,Category,Link,Image,Isactive FROM blogs WHERE isactive='1' ORDER BY ID DESC LIMIT :offset,:quantityOfResults");
                $stmt->bindParam(':quantityOfResults',$quantityOfResultsdb);
                $quantityOfResultsdb=$quantityOfResults;
                $stmt->bindParam(':offset',$offsetdb);
                $offsetdb=$offset;
                
            }
           
        }

        $stmt->execute();
        $result=$stmt->fetchAll();
                 
        var_dump($result);
        return $result;
        
    }
    catch(PDOException $e) {
        $msg="Error: " . $e->getMessage();
        Notifications::Modal("$msg");
    }
    $dbconn = null;

}

/* ------------ get blog by ID from database function ------------------- */
public static function getBlogByID($id){
    global $dbservername,$dbusername,$dbpassword,$dbname;
   
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
            $stmt=$dbconn->prepare("SELECT Heading,Category,Link,Image,Isactive FROM blogs WHERE ID=:id ");
            $stmt->bindParam(':id',$iddb);
            $iddb=$id;
        
        
           
        

        $stmt->execute();
        $result=$stmt->fetchAll();
                 
        $result[0]['Image']=maintain::local_resorces_regulation_address($result[0]['Image'],'blog');
        return $result[0];
        
    }
    catch(PDOException $e) {
        $msg="Error: " . $e->getMessage();
        Notifications::Modal("$msg");
    }
    $dbconn = null;

}



/*------------------------------------blog list pagination function ----------------------------------------------------*/

    public static function blogPagination($category,$justActives,$quantityOfResults){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        try{
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if($justActives==true && $category=='all'){
                $stmt=$dbconn->prepare("SELECT COUNT(ID) FROM blogs WHERE Isactive='1' ");
                
            }elseif($justActives==true && $category!=='all'){
                $stmt=$dbconn->prepare("SELECT COUNT(ID) FROM blogs WHERE Isactive='1' AND Category=:category");
                $stmt->bindParam(':category',$categorydb);
                $categorydb=$category;   
            }elseif($justActives==false && $category=='all'){
                $stmt=$dbconn->prepare("SELECT COUNT(ID) FROM blogs ");
            }elseif($justActives==false && $category!=='all'){
                $stmt=$dbconn->prepare("SELECT COUNT(ID) FROM blogs WHERE Category=:category");
                $stmt->bindParam(':category',$categorydb);
                $categorydb=$category;
            }
            $stmt->execute();
            $result=$stmt->fetchAll();
            $numberOfBlogs=$result['0']['COUNT(ID)'];
            $numberOfpages=ceil($numberOfBlogs/$quantityOfResults);
            return $numberOfpages;
        }  
        catch(PDOException $e) {
            $msg="Error: " . $e->getMessage();
            Notifications::Modal("$msg");
        }
        $dbconn = null;  
    }


/*---------------------------------getting video specification from admin pages and put it on database ----------------*/    
public static function putVideo($link,$heading=null,$duration=null,$comment=null,$jy=null,$jm=null,$jd=null,$active=true,$id=null){
    global $dbservername,$dbusername,$dbpassword,$dbname;
        $date=thirdparty::jalali_to_gregorian($jy, $jm, $jd, $mod='-');
        try{
            
            $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            if($id!==null){
                $stmt=$dbconn->prepare("UPDATE videos SET Link=:link,Heading=:heading,Duration=:duration,Comment=:comment,Date=:date,Active=:active WHERE ID = $id;");
 
            }else{
                $stmt=$dbconn->prepare("INSERT INTO videos(Link,Heading,Duration,Comment,Date,Active) VALUES(:link,:heading,:duration,:comment,:date,:active)");
            }
            $stmt->bindParam(':link',$linkdb);
            $stmt->bindParam(':heading',$headingdb);
            $stmt->bindParam(':duration',$durationdb);
            $stmt->bindParam(':comment',$commentdb);
            $stmt->bindParam(':date',$datedb);
            $stmt->bindParam(':active',$activedb);
            $linkdb=$link;
            $headingdb=$heading;
            $durationdb=$duration;
            $commentdb=$comment;
            $datedb=$date;
            $activedb=$active;
            $stmt->execute();

            Notifications::Modal("تغییرات با موفقیت انجام شد!");

            
        }
        catch(PDOException $e){
            echo ($e);
             

        }
        $dbconn=null;
        
    
}    


/* ------------ get video specifications list from database function ------------------- */
public static function getVideoList($justActives,$quantityOfResults="10",$rowOfResult="1"){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    $offset=($rowOfResult-1)*$quantityOfResults;
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
            switch($justActives){
                case false:
                    $stmt=$dbconn->prepare("SELECT ID,Link,Heading,Duration,Comment,Date,Active FROM videos ORDER BY ID DESC LIMIT :offset,:quantityOfResults ");
                    $stmt->bindParam(':quantityOfResults',$quantityOfResultsdb);
                    $stmt->bindParam(':offset',$offsetdb);
                    $quantityOfResultsdb=$quantityOfResults;
                    $offsetdb=$offset;
                    
                    break;
                default:
                $stmt=$dbconn->prepare("SELECT ID,Link,Heading,Duration,Comment,Date,Active FROM videos WHERE Active='1' ORDER BY ID DESC LIMIT :offset,:quantityOfResults ");
                $stmt->bindParam(':quantityOfResults',$quantityOfResultsdb);
                $quantityOfResultsdb=$quantityOfResults;
                $stmt->bindParam(':offset',$offsetdb);
                $offsetdb=$offset;
                
            }
           
        

        $stmt->execute();
        $result=$stmt->fetchAll();
        foreach($result as $row=>$output){
            
            $test_arr=explode('-',$output['Date']);
            $testDate=thirdparty::gregorian_to_jalali($test_arr[0], $test_arr[1], $test_arr[2], $mod='-');
            $result[$row]['Date']=$testDate;
            if($result[$row]['Active']==1){$result[$row]['Active']='فعال';}else{$result[$row]['Active']='غیر فعال';}
        }        
        
        return $result;
        
        
    }
    catch(PDOException $e) {
        $msg="Error: " . $e->getMessage();
        Notifications::Modal("$msg");
    }
    $dbconn = null;

}


/*------------------------------------video list pagination function ----------------------------------------------------*/

public static function videoPagination($justActives,$quantityOfResults){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    try{
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if($justActives==true){
            $stmt=$dbconn->prepare("SELECT COUNT(ID) FROM videos WHERE Active='1' ");
            
        
        }else{
            $stmt=$dbconn->prepare("SELECT COUNT(ID) FROM videos ");
            
        }
        $stmt->execute();
        $result=$stmt->fetchAll();
        $numberOfBlogs=$result['0']['COUNT(ID)'];
        $numberOfpages=ceil($numberOfBlogs/$quantityOfResults);
        return $numberOfpages;
    }  
    catch(PDOException $e) {
        $msg="Error: " . $e->getMessage();
        Notifications::Modal("$msg");
    }
    $dbconn = null;  
}


/* ------------ get video by ID from database function ------------------- */
public static function getVideoByID($id){
    global $dbservername,$dbusername,$dbpassword,$dbname;
   
    try {
        $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $stmt=$dbconn->prepare("SELECT Heading,Link,Duration,Comment,Date,Active FROM videos WHERE ID=:id ");
        $stmt->bindParam(':id',$iddb);
        $iddb=$id;
        $stmt->execute();
        $result=$stmt->fetchAll();
        
        $test_arr=explode('-',$result[0]['Date']);
        $DateArr=thirdparty::gregorian_to_jalali($test_arr[0], $test_arr[1], $test_arr[2]);         
        $result[0]['Date']=$DateArr;
        $result[0]['Link']=htmlspecialchars($result[0]['Link']);
        return $result[0];
        
    }
    catch(PDOException $e) {
        $msg="Error: " . $e->getMessage();
        Notifications::Modal("$msg");
    }
    $dbconn = null;

}

/* ------------ get title from admin form & put to database function ------------------- */

public static function authenticateUser($username,$password){
    global $dbservername,$dbusername,$dbpassword,$dbname;
    
        try{
            
            $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("SELECT * FROM acounts WHERE Name=:name AND Password=:password");
            $stmt->bindParam(':name',$username);
            $stmt->bindParam(':password',$password);
            $stmt->execute();
            $result=$stmt->fetchAll();
            
            if(!empty($result)){

                header("location:admin/admin-indexpage.php");
                session_start();
                $_SESSION['eligible']=true;
            }else{
                echo "login failed!";
            }
            
        }
        catch(PDOException $e){
            Notifications::Modal($e->getMessage());

        }
        $dbconn=null;
        
    }





}// end of database class




/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------uploading files class --------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/

class File extends database{

 /* ------------ upload file and put file name and path to database function ------------------ */
        public static function putImages($image,$whereToUse,$maxFileSize,$folder,$alt,$shouldReplace){
            
            if($_FILES[$image]["name"]!==''){
            $targetDir=$_SERVER['DOCUMENT_ROOT'].'/hoomanpage/upload/'.$folder."/";
            $targetFile=$targetDir.basename($_FILES[$image]["name"]);
            $uploadOk=1;
            $imageFileType=strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
            
    
            
            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])){
                $check=getimagesize($_FILES["$image"]["tmp_name"]);
                if($check !== false){
                    $uploadOk=1;
                }else{
                    Notifications::Modal("خطا! فایل شما یک تصویر نیست");
                    $uploadOk=0;
                }
            }
    
            // Check if file already exists
            if(file_exists($targetFile)){
                Notifications::Modal("فایل تکراریست اما تصویر عوض می شود!");
                
            }
    
            // Check file size
            if($_FILES[$image]["size"]>$maxFileSize){
                Notifications::Modal("خطا! حجم فایل بیشتر از حد مجاز است");
                $uploadOk=0;
            }
    
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif"){
                Notifications::Modal("خطا! فرمت فایل قابل قبول نیست");
                $uploadOk=0;
                
            }
    
            // Check if $uploadOk is set to 0 by an error
            if($uploadOk==0){
                Notifications::Modal("خطا! متاسفانه فایل شما آپلود نشد");
                
            }else{
                if(move_uploaded_file($_FILES[$image]["tmp_name"],$targetFile)){
                   $msg="فایل".basename($_FILES[$image]["name"])."با موفقیت آپلود شد";
                   Notifications::Modal($msg);
                   
                }else{
                    Notifications::Modal("خطا! متاسفانه فایل شما آپلود نشد");
                    
                }
            }
            
    
    
    
            // put image name and path to database 
            global $dbservername,$dbusername,$dbpassword,$dbname;
            if(isset($targetFile) && $uploadOk==1){
                
                try{
                    $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                    $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                    if($shouldReplace==true){
                        $stmt=$dbconn->prepare("DELETE FROM images WHERE WhereToUse=:wheretouse");
                        $stmt->bindParam(':wheretouse',$wheretousedb);
                        $wheretousedb=$whereToUse;
                        $stmt->execute();
                        
                    };
                    $stmt=$dbconn->prepare("INSERT INTO images(WhereToUse,File,Alt) VALUES(:wheretouse, :targetfile,:alt)");
                    $stmt->bindParam(':wheretouse',$wheretousedb);
                    $stmt->bindParam(':targetfile',$targetfiledb);
                    $stmt->bindParam(':alt',$altdb);
                    $wheretousedb=$whereToUse;
                    $targetfiledb=$targetFile;
                    $altdb=$alt;
                    $stmt->execute();
                    Notifications::Modal("فایل شما با موفقیت آپلود شد!");
                }
                catch(PDOException $e){
                    $msg="Error: " . $e->getMessage();
                    Notifications::Modal("$msg");
                }
                $dbconn=null;
                
            }
        
        }
        }

        /* ------------ get image path and name from database function ------------------- */
        public static function getImage($whereToUse,$folder){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        try {
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt=$dbconn->prepare("SELECT File,Alt,ID FROM images WHERE WhereToUse=:wheretouse");
            $stmt->bindParam(':wheretouse',$wheretousedb);
            $wheretousedb=$whereToUse;
            $stmt->execute();
            $result=$stmt->fetchAll();
                     
            
            return $result;
            
        }
        catch(PDOException $e) {
            $msg="Error: " . $e->getMessage();
            Notifications::Modal("$msg");
        }
        $dbconn = null;
    
    }

/*----------------------------------delete image from folder and database ------------------------------------------*/
public static function deleteImage($id){

    global $dbservername,$dbusername,$dbpassword,$dbname;
        try {
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //get image address and delete file from server
            $stmt=$dbconn->prepare("SELECT File FROM images WHERE ID=:id");
            $stmt->bindParam(':id',$iddb);
            $iddb=$id;
            $stmt->execute();
            $result=$stmt->fetchAll();
            $fileAddress=$result['0']['File'];
            unlink($fileAddress);
            //delete image from database
            $stmt=$dbconn->prepare("DELETE FROM images WHERE ID=:id");
            $stmt->bindParam(':id',$iddb);
            $iddb=$id;
            $stmt->execute();
            
        }
        catch(PDOException $e) {
            $msg="Error: " . $e->getMessage();
            Notifications::Modal("$msg");
        }
        $dbconn = null;
        
        echo "<meta http-equiv='refresh' content='0'>";
}
  

/*----------------------------- get blog detail & image from admin and upload image and put data in DB-----------------------------*/
public static function putBlog($heading,$category,$link,$image,$isActive,$id=null){
    
    
    $link=maintain::modification_input_url($link);
    if(!is_null($image)){
       
        if($_FILES[$image]["name"]!==''){
        $targetDir=$_SERVER['DOCUMENT_ROOT'].'/hoomanpage/upload/blog/';
        $targetFile=$targetDir.basename($_FILES[$image]["name"]);
        $uploadOk=1;
        $imageFileType=strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
        

        
            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])){
                $check=getimagesize($_FILES["$image"]["tmp_name"]);
                if($check !== false){
                    $uploadOk=1;
                }else{
                    Notifications::Modal("خطا! فایل شما یک تصویر نیست");
                    $uploadOk=0;
                }
                
            }

            // Check if file already exists
            if(file_exists($targetFile)){
                Notifications::Modal("فایل تکراریست اما تصویر عوض می شود!");
                
            }

            // Check file size
            if($_FILES[$image]["size"]>2000000){
                Notifications::Modal("خطا! حجم فایل بیشتر از حد مجاز است");
                $uploadOk=0;
            }

            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif"){
                Notifications::Modal("خطا! فرمت فایل قابل قبول نیست");
                $uploadOk=0;
                
            }

            // Check if $uploadOk is set to 0 by an error
            if($uploadOk==0){
                Notifications::Modal("خطا! متاسفانه فایل شما آپلود نشد");
                
            }else{
                if(move_uploaded_file($_FILES[$image]["tmp_name"],$targetFile)){
                $msg="فایل".basename($_FILES[$image]["name"])."با موفقیت آپلود شد";
                Notifications::Modal($msg);
                
                }else{
                    Notifications::Modal("خطا! متاسفانه فایل شما آپلود نشد");
                    
                }
            }
        }
    }       



    // put image name and path to database 
    global $dbservername,$dbusername,$dbpassword,$dbname;
    
        
        try{
            $dbconn=new PDO("mysql:host=$dbservername;dbname=$dbname",$dbusername,$dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            if (is_null($id)){
                $stmt=$dbconn->prepare("INSERT INTO blogs(Heading,Category,Link,Image,Isactive) VALUES(:heading,:category,:link,:image,:isactive)");
                $stmt->bindParam(':image',$imagedb);
                $imagedb=$targetFile;
            }else{
                if(isset($image)){
                    $stmt=$dbconn->prepare("UPDATE blogs SET Heading=:heading,Category=:category,Link=:link,Image=:image,Isactive=:isactive WHERE ID=$id");   
                    $stmt->bindParam(':image',$imagedb); 
                    $imagedb=$targetFile;
                }if(!isset($image)){
                    $stmt=$dbconn->prepare("UPDATE blogs SET Heading=:heading,Category=:category,Link=:link,Isactive=:isactive WHERE ID=$id");
                                    
                }    
            }
            $stmt->bindParam(':heading',$headingdb);
            $stmt->bindParam(':category',$categorydb);
            $stmt->bindParam(':link',$linkdb);
            
            $stmt->bindParam(':isactive',$isactivedb);
            
            $headingdb=$heading;
            $categorydb=$category;
            $linkdb=$link;
            $isactivedb=$isActive;
            $stmt->execute();
            
            Notifications::Modal("فایل شما با موفقیت آپلود شد!");
            
        }
        catch(PDOException $e){
            $msg="Error: " . $e->getMessage();
            Notifications::Modal("$msg");
            
        }
        $dbconn=null;
        
    



    
    }


    


    
} // end of File class
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ------------------------------------------  necessary changes to admin pages from database contents ------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
class AdminPage extends database{
    // check activation of topmenu pages
    public static function getActivePages($page){
        global $dbservername,$dbusername,$dbpassword,$dbname;
        try {
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT Active FROM topnavigation WHERE Url='$page'";
            $stmt=$dbconn->query($sql);
            $result=$stmt->fetch($dbconn::FETCH_ASSOC);
            $descriptionResult=$result['Active'];
            $descriptionResult=str_replace('"',"&#34",$descriptionResult);
            $descriptionResult=str_replace("'","&#39",$descriptionResult);
            return $descriptionResult;
            
        }
        catch(PDOException $e) {
            Notifications::Modal("$sql.<br/>.$e->getMessage()");
        }
        $dbconn = null;
    }
}

/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------notifications class ----------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
class Notifications{
    // modal for user function
    public static function Modal($msg){
      echo '<div id="myModal" class="modal"
      style="display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */"
    >
  
      <!-- Modal content -->
      <div class="modal-content" style="
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 30%; /* Could be more or less, depending on screen size */
            text-align:center;
      ">
        <span class="close-modal" style="
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
            cursor:pointer;
        ">&times;</span>
        <p style="font-size:20px; padding:15px;">'.$msg.'</p>
      </div>
  
    </div>
    <script>
            // Get the modal
        var modal = document.getElementById("myModal");
  
        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");
  
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close-modal")[0];
  
        // When the user clicks on the button, open the modal
        modal.style.display = "block";
        
        var timePeriodInMs = 2000;
        setTimeout(function() 
        { 
            modal.style.display = "none"; 
        }, 
        timePeriodInMs);
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
        modal.style.display = "none";
        }
  
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
        }   
    </script>' ;
    }
    } // end of notification class

/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------scurity class ----------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
class security{
public static function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
}  



// end of security class
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------maintain class ----------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/


class maintain{

    // a function to change src address of images to solve prohibiting browser to upload from local resources 
    public static function local_resorces_regulation_address($url,$folder){
        $optsource="http://".$_SERVER['SERVER_NAME']."/hoomanpage/upload/"."$folder"."/".basename($url);
        return $optsource;      
    }
    

    // a function to get input url and modification it to show properly in html
    public static function modification_input_url($inputUrl){
        $inputUrl=trim($inputUrl,'/');
        if(!preg_match('#^http(s)?://#',$inputUrl)){
            $inputUrl='http://'.$inputUrl;
        }
        if($inputUrl=="http://"){$inputUrl='';};
        return $inputUrl;
    }




}



// end of maintain class 



/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------third party class ----------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
 class thirdparty{
/**  Gregorian & Jalali (Hijri_Shamsi,Solar) Date Converter Functions
Author: JDF.SCR.IR =>> Download Full Version :  http://jdf.scr.ir/jdf
License: GNU/LGPL _ Open Source & Free :: Version: 2.80 : [2020=1399]
---------------------------------------------------------------------
355746=361590-5844 & 361590=(30*33*365)+(30*8) & 5844=(16*365)+(16/4)
355666=355746-79-1 & 355668=355746-79+1 &  1595=605+990 &  605=621-16
990=30*33 & 12053=(365*33)+(32/4) & 36524=(365*100)+(100/4)-(100/100)
1461=(365*4)+(4/4) & 146097=(365*400)+(400/4)-(400/100)+(400/400)  */

public static function gregorian_to_jalali($gy, $gm, $gd, $mod='') {
    $g_d_m = array(0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334);
    $gy2 = ($gm > 2)? ($gy + 1) : $gy;
    $days = 355666 + (365 * $gy) + ((int)(($gy2 + 3) / 4)) - ((int)(($gy2 + 99) / 100)) + ((int)(($gy2 + 399) / 400)) + $gd + $g_d_m[$gm - 1];
    $jy = -1595 + (33 * ((int)($days / 12053)));
    $days %= 12053;
    $jy += 4 * ((int)($days / 1461));
    $days %= 1461;
    if ($days > 365) {
      $jy += (int)(($days - 1) / 365);
      $days = ($days - 1) % 365;
    }
    if ($days < 186) {
      $jm = 1 + (int)($days / 31);
      $jd = 1 + ($days % 31);
    } else{
      $jm = 7 + (int)(($days - 186) / 30);
      $jd = 1 + (($days - 186) % 30);
    }
    return ($mod == '')? array($jy, $jm, $jd) : $jy.$mod.$jm.$mod.$jd;
  }
  
  public static function jalali_to_gregorian($jy, $jm, $jd, $mod='') {
    $jy += 1595;
    $days = -355668 + (365 * $jy) + (((int)($jy / 33)) * 8) + ((int)((($jy % 33) + 3) / 4)) + $jd + (($jm < 7)? ($jm - 1) * 31 : (($jm - 7) * 30) + 186);
    $gy = 400 * ((int)($days / 146097));
    $days %= 146097;
    if ($days > 36524) {
      $gy += 100 * ((int)(--$days / 36524));
      $days %= 36524;
      if ($days >= 365) $days++;
    }
    $gy += 4 * ((int)($days / 1461));
    $days %= 1461;
    if ($days > 365) {
      $gy += (int)(($days - 1) / 365);
      $days = ($days - 1) % 365;
    }
    $gd = $days + 1;
    $sal_a = array(0, 31, (($gy % 4 == 0 and $gy % 100 != 0) or ($gy % 400 == 0))?29:28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    for ($gm = 0; $gm < 13 and $gd > $sal_a[$gm]; $gm++) $gd -= $sal_a[$gm];
    return ($mod == '')? array($gy, $gm, $gd) : $gy.$mod.$gm.$mod.$gd;
  }
  

} // end of thirdparty class

/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------account class ----------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------------------------*/

class account{
    private $id;
	private $name;
    private $authenticated;
    
    public function __construct()
	{
		/* Initialize the $id and $name variables to NULL */
		$this->id = NULL;
		$this->name = NULL;
		$this->authenticated = FALSE;
	}
	
	/* Destructor */
	public function __destruct()
	{
		
    }
    public static function addAccount(string $name, string $passwd): int{
        global $dbservername,$dbusername,$dbpassword,$dbname;
        $name=trim($name);
        $passwd=trim($passwd);
        if(!$this->isNameValid($name)){
            throw new Exception('Invalid user name');
        }
        if(!$this->isPasswdValid($passwd)){
            throw new Exception('Invalid password');
        }
        if(!is_null($this->getIdFromName($name))){
            throw new Exception('User name not available!');
        }
        try {
         
            $dbconn = new PDO("mysql:host=$dbservername;dbname=$dbname", $dbusername, $dbpassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'INSERT INTO accounts (Name, Password) VALUES (:name, :passwd)';
            $hash=password_hash($passwd,PASSWORD_DEFAULT);
            $values=array(':name'=>$name,':passwd'=>$hash);
            $res=$dbconn->prepare($sql);
            $res->execute($values);
            
        }
        catch(PDOException $e) {
            Notifications::Modal("$sql.<br/>.$e->getMessage()");
        }
        $dbconn = null;
        return $dbconn->lastInsertid();
    }
}// end of account class

?>