

// opening responsive menu function

function openmenu(){
    
    var menu=document.getElementById("responsive-menu");
    var goleft=setInterval(menuanimate,1);
    var leftvalue=100;
    function menuanimate(){
    if(leftvalue==0){
        clearInterval(goleft);
    }
    else{
        leftvalue --;
        menu.style.left=leftvalue + "%";
    }
    }
}
function closemenu(){
    
    var menu=document.getElementById("responsive-menu");
    var goright=setInterval(menuanimate,1);
    var leftvalue=0;
    function menuanimate(){
    if(leftvalue==100){
        clearInterval(goright);
    }
    else{
        leftvalue ++;
        menu.style.left=leftvalue + "%";
    }
    }

}

// dashboard accordion right sidebar
var acc=document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      } 
    });
  }

//get query string of admin-blogpage.php
function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  var returnNumber=decodeURIComponent(results[2].replace(/\+/g, ' '));
   
  return returnNumber;
}
var currentPagination=getParameterByName('pageination');

/*-------------------admin-blogpage.php-----------*/

//change background of active pageination 
if(currentPagination==null){currentPagination=1;}
var activeBlogID='pageination'+currentPagination;
document.getElementById(activeBlogID).style.backgroundColor = "#f26447";
document.getElementById(activeBlogID).style.color = "white";
document.getElementById(activeBlogID).style.borderColor = "#f26447";
document.getElementById(activeBlogID).removeAttribute("href");




