-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 18, 2020 at 09:39 PM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hoomanpage`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_sessions`
--

DROP TABLE IF EXISTS `account_sessions`;
CREATE TABLE IF NOT EXISTS `account_sessions` (
  `SessionID` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `AccountID` int(10) UNSIGNED NOT NULL,
  `LoginTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`SessionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acounts`
--

DROP TABLE IF EXISTS `acounts`;
CREATE TABLE IF NOT EXISTS `acounts` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_bin NOT NULL,
  `Password` varchar(255) COLLATE utf8_bin NOT NULL,
  `RegTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUE` (`Name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `acounts`
--

INSERT INTO `acounts` (`ID`, `Name`, `Password`, `RegTime`, `Enabled`) VALUES
(1, 'hooman_the1', 'Hooman61', '2020-05-17 21:47:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Heading` varchar(1000) COLLATE utf8_bin NOT NULL,
  `Category` varchar(500) COLLATE utf8_bin NOT NULL,
  `Link` varchar(1000) COLLATE utf8_bin NOT NULL,
  `Image` varchar(1000) COLLATE utf8_bin NOT NULL,
  `Isactive` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`ID`, `Heading`, `Category`, `Link`, `Image`, `Isactive`) VALUES
(1, ' edited', 'edited', 'http://edit.net', 'C:/wamp64/www/hoomanpage/upload/blog/hooman.jpg', 1),
(6, 'edited', 'edited', 'https://www.atttlas.com/NewAtlas/public/admin/edit-post.php?post_id=73161', 'C:/wamp64/www/hoomanpage/upload/blog/hooman.jpg', 1),
(5, 'edited', 'edited', 'https://www.edited.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(7, '12عنوان edited', 'sdafdas', 'http://3.com', 'C:/wamp64/www/hoomanpage/upload/blog/total.jpg', 1),
(33, 'مقاله 1', 'موضوع 4 ', 'https://www.atttlas.com/NewAtlas/public/admin/edit-post.php?post_id=73161', 'C:/wamp64/www/hoomanpage/upload/blog/chall.jpg', 1),
(8, 'مقاله 5 ', 'موضوع 5', 'https://www.atttlas.com/NewAtlas/public/admin/edit-post.php?post_id=73161', 'C:/wamp64/www/hoomanpage/upload/blog/0000_mars_landing.jpg', 0),
(9, '9عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(10, '10عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(11, 'عنوان11', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(12, '12عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(13, '13عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(14, '14عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(15, '15عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(16, '16عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(17, '17عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(18, '18عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(19, '19عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(20, '20عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(21, '21عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(22, '22عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(23, '23عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(24, '24عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(25, '25عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(26, '26عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(27, '27عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(28, '28عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(29, 'عنوان', 'موضوع', 'https://stackoverflow.com', 'C:/wamp64/www/hoomanpage/upload/blog/iss.jpg', 1),
(30, 'مقاله 2', 'موضوع 4', 'https://www.atttlas.com/NewAtlas/public/admin/edit-post.php?post_id=73161', 'C:/wamp64/www/hoomanpage/upload/blog/total.jpg', 1),
(31, 'مقاله 1', 'موضوع 4 ', 'https://www.atttlas.com/NewAtlas/public/admin/edit-post.php?post_id=73161', 'C:/wamp64/www/hoomanpage/upload/blog/chall.jpg', 1),
(32, 'مقاله 1', 'موضوع 4 ', 'https://www.atttlas.com/NewAtlas/public/admin/edit-post.php?post_id=73161', 'C:/wamp64/www/hoomanpage/upload/blog/chall.jpg', 1),
(34, 'no', 'no', 'https://www.atttlas.com', 'C:/wamp64/www/hoomanpage/upload/blog/chall.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contactform`
--

DROP TABLE IF EXISTS `contactform`;
CREATE TABLE IF NOT EXISTS `contactform` (
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Family` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Message` varchar(10000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactform`
--

INSERT INTO `contactform` (`Name`, `Family`, `Email`, `Subject`, `Message`, `id`) VALUES
('هومن', 'محمودی', 'hooman_the1@yahoo.com', 'اکانت گوگل ادوردز', 'sdad', 27),
('هومن', 'محمودی', 'hooman_the1@yahoo.com', 'اکانت گوگل ادوردز', 'sdad', 28),
('هومن', 'محمودی', 'hooman_the1@yahoo.com', 'افزایش اعتبار', 'زشسزسش', 29),
('هومن', 'محمودی', 'hooman_the1@yahoo.com', 'تغییر سرور', 'wsdasf', 30);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Customer` varchar(255) COLLATE utf8_bin NOT NULL,
  `Link` varchar(1000) COLLATE utf8_bin NOT NULL,
  `Comment` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`ID`, `Customer`, `Link`, `Comment`) VALUES
(34, 'دکتر شعبان', 'http://drmandanashaban.com', NULL),
(31, 'هانی کاشانی', 'http://kashanielectric.com', NULL),
(4, 'هومن محمودی', 'http://atttlas.com', NULL),
(35, 'فربد شیرالی', 'http://farbodcar.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `descriptiontags`
--

DROP TABLE IF EXISTS `descriptiontags`;
CREATE TABLE IF NOT EXISTS `descriptiontags` (
  `Page` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `DescriptionValue` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Comment` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`Page`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `descriptiontags`
--

INSERT INTO `descriptiontags` (`Page`, `DescriptionValue`, `Comment`) VALUES
('index.php', 'هومن ممتحنی یه طراح وب سایت ', NULL),
('contactus.php', 'در این صفحه در مورد خودم &#34هومن ممتحنی&#34 توضیح میدم براتون', NULL),
('aboutus.php', 'در این &#39صفحه&#39 در مورد خودم &#34هومن ممتحنی&#34 توضیح میدم براتون', NULL),
('blog.php', 'در اینجا مقالاتی که نوشتم یا ترجمه کردم رو در اختیارتون میزارم ', NULL),
('multimedia.php', 'در این صفحه در مورد ویدئوهای خودم &#34هومن ممتحنی&#34 توضیح میدم براتون', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `WhereToUse` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `File` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Alt` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`WhereToUse`, `File`, `Alt`, `ID`) VALUES
('about1image', 'C:/wamp64/www/hoomanpage/upload/aboutus/moon.jpg', 'تفغتفغ', 108),
('imagebox1', 'C:/wamp64/www/hoomanpage/upload/aboutus/earth.png', '2222', 110),
('indexpage', 'C:/wamp64/www/hoomanpage/upload/index/ai-product.jpg', '4', 91),
('indexpage', 'C:/wamp64/www/hoomanpage/upload/index/ai1.jpg', '2', 90),
('headerimage', 'C:/wamp64/www/hoomanpage/upload//hooman.jpg', 'هومن ممتحنی', 115),
('indexpage', 'C:/wamp64/www/hoomanpage/upload/index/Capture.PNG', '', 87),
('indexpage', 'C:/wamp64/www/hoomanpage/upload/index/oQBuw.gif', 'sdz', 88),
('indexpage', 'C:/wamp64/www/hoomanpage/upload/index/0000_mars_landing.jpg', '1', 89),
('about2image', 'C:/wamp64/www/hoomanpage/upload/aboutus/ai1.jpg', '111', 114),
('imagebox2', 'C:/wamp64/www/hoomanpage/upload/aboutus/iss.jpg', 'hdffh', 112),
('indexpage', 'C:/wamp64/www/hoomanpage/upload/index/AIjpg.png', 'uky', 107),
('aboutmidimage', 'C:/wamp64/www/hoomanpage/upload/aboutus/total.jpg', 'jfgj', 113),
('indexpage', 'C:/wamp64/www/hoomanpage/upload/index/MzQyMjg5Mw.jpeg', 'adaasd', 103);

-- --------------------------------------------------------

--
-- Table structure for table `keywordtags`
--

DROP TABLE IF EXISTS `keywordtags`;
CREATE TABLE IF NOT EXISTS `keywordtags` (
  `Page` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `KeywordValue` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Comment` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`Page`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keywordtags`
--

INSERT INTO `keywordtags` (`Page`, `KeywordValue`, `Comment`) VALUES
('aboutus.php', 'هومن ممتحنی', NULL),
('index.php', 'هومن ممتحنی', NULL),
('contactus.php', 'sddfsfg sfffasdd sdgsdgs', NULL),
('blog.php', 'مقالات هومن ممتحنی، مقاله، ترجمه،هومن ممتحنی', NULL),
('multimedia.php', 'ویدئو', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
  `WhereToUse` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Link` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=186 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`WhereToUse`, `Link`, `ID`) VALUES
('aboutusBox2', 'http://farbodcar.com', 185),
('aboutusBox1', '', 184);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`Email`, `Date`) VALUES
('momtaheni@atttlas.com', '2020-03-21'),
('vcvcv@attlas.com', '2020-03-21'),
('hooman.the110@gmail.com', '2020-03-21'),
('fsfsd@sdfsf.sdsd', '2020-03-21'),
('hooman_the1@yahoo.com', '2020-03-21'),
('hooman_the1@yahoo.comp', '2020-03-28'),
('hooman_the1@yahoo.comsd', '2020-03-30');

-- --------------------------------------------------------

--
-- Table structure for table `pagetextcontent`
--

DROP TABLE IF EXISTS `pagetextcontent`;
CREATE TABLE IF NOT EXISTS `pagetextcontent` (
  `Wheretouse` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pagetextcontent`
--

INSERT INTO `pagetextcontent` (`Wheretouse`, `Description`, `heading`) VALUES
('contactus', '\r\n sddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgssddfsfg sfffasdd sdgsdgs', 'sddfsfg sfffasdd sdgsdgs'),
('aboutusBox1', 'fsdfsadfgsdgsdfgdf&lt;&gt;&quot;&quot;jsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3goipoweiuropwe;ortoipo\r\n\r\njsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3goipoweiuropwe;ortoipo\r\n\r\njsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3goipoweiuropwe;ortoipo\r\n\r\njsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3goipoweiuropwe;ortoipo\r\n\r\n', 'تیتر باکس اول'),
('aboutusBox2', 'jsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3gjsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3goipoweiuropwe;ortoipo\r\n\r\njsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3goipoweiuropwe;ortoipo\r\n\r\njsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3goipoweiuropwe;ortoipo\r\n\r\njsjjjjjsl;kc;slak;lska;lsfk;lsaksejoiwuyoeiri3goipoweiuropwe;ortoipo\r\n\r\noipoweiuropwe;ortoipo', 'gdfgdf'),
('index', 'هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت هومن ممتحنی یه طراح وب سایت ', '');

-- --------------------------------------------------------

--
-- Table structure for table `socialaddress`
--

DROP TABLE IF EXISTS `socialaddress`;
CREATE TABLE IF NOT EXISTS `socialaddress` (
  `Social` varchar(255) COLLATE utf8_bin NOT NULL,
  `Address` varchar(1000) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `socialaddress`
--

INSERT INTO `socialaddress` (`Social`, `Address`) VALUES
('instagram', 'https://www.instagram.com/hooman_momtaheni/'),
('linkedin', 'https://www.linkedin.com/in/h-momtaheni/');

-- --------------------------------------------------------

--
-- Stand-in structure for view `test1`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `test1`;
CREATE TABLE IF NOT EXISTS `test1` (
);

-- --------------------------------------------------------

--
-- Table structure for table `titletags`
--

DROP TABLE IF EXISTS `titletags`;
CREATE TABLE IF NOT EXISTS `titletags` (
  `TitleValue` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Page` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `titletags`
--

INSERT INTO `titletags` (`TitleValue`, `Comment`, `Page`) VALUES
(' ', ' ', ' '),
('درباره من', NULL, 'aboutus.php'),
('صفحه اصلی', NULL, 'index.php'),
('کارشناس شبکه های اجتماعی', NULL, 'contactus.php'),
('لیست مقالات', NULL, 'blog.php'),
('ویدئوهای من', NULL, 'multimedia.php');

-- --------------------------------------------------------

--
-- Table structure for table `topnavigation`
--

DROP TABLE IF EXISTS `topnavigation`;
CREATE TABLE IF NOT EXISTS `topnavigation` (
  `ID` int(11) DEFAULT NULL,
  `PageName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Active` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topnavigation`
--

INSERT INTO `topnavigation` (`ID`, `PageName`, `Url`, `Active`, `Comment`) VALUES
(5, 'خانه', 'index.php', 'checked', NULL),
(1, 'تماس با من', 'contactus.php', 'checked', NULL),
(2, 'درباره من', 'aboutus.php', 'checked', NULL),
(3, 'بلاگ', 'blog.php', 'checked', NULL),
(4, 'مولتی مدیا', 'multimedia.php', 'checked', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE IF NOT EXISTS `videos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Link` varchar(1000) COLLATE utf8_bin NOT NULL,
  `Heading` varchar(2000) COLLATE utf8_bin DEFAULT NULL,
  `Duration` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `Comment` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `Date` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`ID`, `Link`, `Heading`, `Duration`, `Comment`, `Date`, `Active`) VALUES
(81, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(80, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(79, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(78, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(77, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(76, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(75, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(74, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(73, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(72, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '4 دقیقه', 'ساختن مدل آزمایشی با استفاده از وب سایت', '2018-4-4', 1),
(70, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', '9 دقیقه', 'آیا با وجود اینستاگرام و تلگرام و سایر شبکه های اجتماعی برای رونق کسب و کار نیازی به هزینه کردن برای وب سایت داریم؟', '2017-3-21', 1),
(71, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'معرفی اطلس ساختار داده', 'کمتر از 1 دقیقه', 'آیا با وجود اینستاگرام و تلگرام و سایر شبکه های اجتماعی برای رونق کسب و کار نیازی به هزینه کردن برای وب سایت داریم؟', '2020-7-6', 1),
(82, '<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class=\"h_iframe-aparat_embed_frame\"><span style=\"display: block;padding-top: 57%\"></span><iframe src=\"https://www.aparat.com/video/video/embed/videohash/lxf29/vt/frame\" allowFullScreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe></div>', 'edit', 'edit', 'edit', '2017-3-21', 1);

-- --------------------------------------------------------

--
-- Structure for view `test1`
--
DROP TABLE IF EXISTS `test1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `test1`  AS  select `indexpagemeta`.`MetaDescription` AS `MetaDescription`,`indexpagemeta`.`MetaID` AS `MetaID`,`indexpagemeta`.`MetaTag` AS `MetaTag` from `indexpagemeta` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
