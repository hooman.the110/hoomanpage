<?php

include $_SERVER['DOCUMENT_ROOT'].'/hoomanpage/functions/functions.php'; 
$titleResult=database::getPageTitle(basename($_SERVER['PHP_SELF'])); 
$keywordResult=database::getKeyword(basename($_SERVER['PHP_SELF'])); 
$descriptionResult=database::getDescription(basename($_SERVER['PHP_SELF'])); 
$headerimage=File::getImage('headerimage','')['0']['File'];
$about1image=File::getImage('about1image','aboutus')['0']['File'];
$about1image=maintain::local_resorces_regulation_address($about1image,'aboutus');
$about1alt=File::getImage('about1image','aboutus')['0']['Alt'];
$about2image=File::getImage('about2image','aboutus')['0']['File'];
$about2image=maintain::local_resorces_regulation_address($about2image,'aboutus');
$about2alt=File::getImage('about2image','aboutus')['0']['Alt'];
$imagebox1=File::getImage('imagebox1','aboutus')[0]['File'];
$imagebox2=File::getImage('imagebox2','aboutus')[0]['File'];
$imagebox1=maintain::local_resorces_regulation_address($imagebox1,'aboutus');
$imagebox2=maintain::local_resorces_regulation_address($imagebox2,'aboutus');
$aboutmidimage=File::getImage('aboutmidimage','aboutus')[0]['File'];
$aboutmidimage=maintain::local_resorces_regulation_address($aboutmidimage,'aboutus');
$aboutmidalt=File::getImage('aboutmidimage','aboutus')[0]['Alt'];
$altimagebox1=File::getImage('imagebox1','aboutus')[0]['Alt'];
$altimagebox2=File::getImage('imagebox2','aboutus')[0]['Alt'];
$navigationPages=database::getNavigationPages();
$instagramAddress=database::getSocialAddress("instagram");
$linkedinAddress=database::getSocialAddress("linkedin");
$aboutusBox1Text=database::getTextContent("aboutusBox1")['Description'];
$aboutusBox1heading=database::getTextContent('aboutusBox1')['heading'];
$aboutusBox1link=database::getLink('aboutusBox1');
$aboutusBox2Text=database::getTextContent("aboutusBox2")['Description'];
$aboutusBox2heading=database::getTextContent('aboutusBox2')['heading'];
$aboutusBox2link=database::getLink('aboutusBox2');
$customers=database::getCustomers();

//get active page number from url string for page
parse_str($_SERVER['QUERY_STRING'],$queries);
if(empty($queries)){$queries['pageination']=1;}
$activePage=$queries['pageination'];

//pageination initializing for blog.php
$numberOfBlogsInPage=10;
$category='all';
$justActiveBlogs=true;
$numberOfBlogPages=database::blogPagination($category,$justActiveBlogs,$numberOfBlogsInPage);
$blogList=database::getBlogList($category,$justActiveBlogs,$numberOfBlogsInPage,$activePage);

//pageination initializing for multimedia.php
$numberOfVideosInPage=10;
$justActiveVideos=true;
$videoList=database::getVideoList($justActiveVideos,$numberOfVideosInPage,$activePage);
$numberOfVideoPages=database::videoPagination($justActiveVideos,$numberOfVideosInPage);





if (isset($_POST['submit'])){
    $userEmail=security::test_input($_POST['email']);
    database::getEmail($userEmail);
}
if(isset($_POST['contact-submit'])){
    $fName=security::test_input($_POST['fname']);
    $lName=security::test_input($_POST['lname']);
    $emailAdd=security::test_input($_POST['emailadd']);
    $subject=security::test_input($_POST['subject']);
    $message=security::test_input($_POST['message']);
    if(filter_var($emailAdd,FILTER_VALIDATE_EMAIL)){
    database::putContactForm($fName,$lName,$emailAdd,$subject,$message);
    }else{
        Notifications::Modal('لطفا یک ایمیل صحیح وارد کنید');
    }
}
$indexTextResult=database::getTextContent('index');
$contactusTextResult=database::getTextContent('contactus');

?>