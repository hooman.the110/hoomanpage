<?php require_once('config.php'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>درباره من</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo baseUrl ; ?>index.css">
        <link href="<?php echo baseUrl ; ?>css/all.css" rel="stylesheet">
        <!-- <script src="jquery/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.mosaic.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery.mosaic.min.css" /> -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo baseUrl ; ?>js/jquery.mosaic.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo baseUrl ; ?>css/jquery.mosaic.min.css" />
        <script type="text/javascript" src="<?php echo baseUrl ; ?>js/index.js"></script>
    </head>
    <body>
    
     
       
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header>
            
            <p class="text">
            <i class="fas fa-bars" id="menu-button" onclick="openmenu()"></i>
            </p>
            <p class="borderline top"></p>  
            <div class="responsive-menu" id="responsive-menu">
                <p><span>آیتم1</span></p>
                <p><span>آیتم2</span></p>
                <p><span>آیتم3</span></p>
                <p><span>آیتم4</span></p>
                <p><span>آیتم5</span></p>
                <p class="close"><i class="fas fa-times" id="close-button" onclick="closemenu()"></i></p> 
            </div> 
            <p class="image">
                <img src="<?php echo baseUrl ; ?>images/hooman.png" alt="هومن ممتحنی">

            </p> 
           
            <div class="wide-menu">
                <span>آیتم 1</span>
                <span>آیتم 2</span>
                <span>آیتم 3</span>
                <span>آیتم 4</span>
                <span>آیتم 5</span>
                
            </div>
            <p class="borderline"></p>
        </header>
        <div id="myMosaic" class="myworks"> 
            <div class="red"></div>
            <div class="blue"></div>
            <div class="green"></div>
            <div class="black"></div>
            <script>
                $('#myMosaic').Mosaic({
                    maxRowHeightPolicy: 'crop',
                });
            </script>
        </div>
        
        <div class="aboutme">
           <p class="text"> لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
           </p>     
        </div>    
        <p class="borderline"></p>
        <footer>
            <div class="subscribe">
                <h3>خبرنامه</h3>
                ایمیلتون رو وارد کنید تا جدیدترین مقالات رو براتون بفرستم
                <form>
                    <input type="text" id="email" name="email">
                    <input type="submit" value="تایید ایمیل">
                </form>
            </div>
            <p class="copyright">
                استفاده از مطالب با ذکر منبع مجاز است
            </p>
            <p class="social">
            <a href="https://www.instagram.com/hooman_momtaheni/"><i class="fab fa-instagram"></i></a>
            <a href="https://ir.linkedin.com/in/h-momtaheni" target="_blank"><i class="fab fa-linkedin"></i></a>
            </p>
        </footer>
    </body>
</html>